function cal(h , m) {

  if(h ==12) h =0;
  if(m ==60) m =0;
  if(h>12 || h<0 || m>60 || m<0){
    return 'wrong input'
    //console.log('worng input')
  }

  var hour_angle = 0.5 *(h*60 + m);
  var minute_angle = 6*m;
  var angle = Math.abs(hour_angle - minute_angle);
   // Return the smaller angle of two possible angles
   angle = Math.min(360-angle, angle);
   //console.log(angle);
   return angle

}
console.log(cal(3,0));
console.log(cal(4,0));
console.log(cal(12,61));
console.log(cal(1,60));
